#include "UART.h"
#include "REG52.H"

/// @brief 向串口发送一个数据
/// @param Byte
void Uart_SendByte(unsigned char Byte)
{
    SBUF = Byte;
    while (TI == 0);
    TI = 0;
}

/// @brief 串口初始化
/// @param
void Uart_Init(void) // 4800bps@12.000MHz
{
    PCON |= 0x80; // 使能波特率倍速位SMOD
    SCON = 0x50;  // 8位数据,可变波特率
    // AUXR &= 0xBF;		//定时器时钟12T模式
    // AUXR &= 0xFE;		//串口1选择定时器1为波特率发生器
    TMOD &= 0x0F; // 设置定时器模式
    TMOD |= 0x20; // 设置定时器模式
    TL1 = 0xF3;   // 设置定时初始值
    TH1 = 0xF3;   // 设置定时重载值
    ET1 = 0;      // 禁止定时器中断
    TR1 = 1;      // 定时器1开始计时

    EA = 1;
    ES = 1;
}

#if 0

// 电脑向单片机发送数据
void UART_Routine(void) interrupt 4
{
    if (RI == 1) {
        P2 = ~SBUF;
        Uart_SendByte(SBUF);
        RI = 0;
    }
}

// 单片机向电脑发送数据
void Uart_SendByte(unsigned char Byte)
{
    SBUF = Byte;
    while (TI == 0);
    TI = 0;
}
#endif
