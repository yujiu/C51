#ifndef __I2C_H__
#define __I2C_H__

void I2C_Start(void);
void I2C_Stop(void);
bit I2C_ReceiveAck(void);
void I2C_SendAck(bit Ackbit);
unsigned char I2C_ReceiveByte(void);
void I2C_SendByte(unsigned char Byte);

#endif