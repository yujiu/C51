#ifndef __NIXIE_H__
#define __NIXIE_H__

extern void Nixie(unsigned char Location, Number);
void Nixie_Loop();
void Nixie_SetBuf(unsigned char Location, Number);

#endif
