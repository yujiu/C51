#include "REG52.H"
#include "key.h"
#include "Nixie.h"
#include "Timer0.h"
#include "AT24C02.h"
#include "Delay.h"

unsigned char KeyNum;
unsigned char min, sec, miniSec;
unsigned char RunFlag;

void main()
{
    Timer0_Init();

    while (1) {
        KeyNum = Key();
        if (KeyNum == 1) {
            RunFlag = !RunFlag;
        }
        if (KeyNum == 2) {
            min     = 0;
            miniSec = 0;
            sec     = 0;
        }
        if (KeyNum == 3) {
            AT24C02_WriteByte(0, min);
            Delay(5);
            AT24C02_WriteByte(1, sec);
            Delay(5);
            AT24C02_WriteByte(2, miniSec);
            Delay(5);
        }
        if (KeyNum == 4) {
            min     = AT24C02_ReadByte(0);
            sec     = AT24C02_ReadByte(1);
            miniSec = AT24C02_ReadByte(2);
        }
        Nixie_SetBuf(1, min / 10);
        Nixie_SetBuf(2, min % 10);
        Nixie_SetBuf(3, 11);
        Nixie_SetBuf(4, sec / 10);
        Nixie_SetBuf(5, sec % 10);
        Nixie_SetBuf(6, 11);
        Nixie_SetBuf(7, miniSec / 10);
        Nixie_SetBuf(8, miniSec % 10);
    }
}

void Sec_Loop()
{
    if (RunFlag) {
        miniSec++;
        if (miniSec == 100) {
            miniSec = 0;
            sec++;
            if (sec == 60) {
                sec = 0;
                min++;
                if (min == 60) {
                    min = 0;
                }
            }
        }
    }
}

void Timer0_Routine() interrupt 1
{
    static unsigned int T0Count1, T0Count2, T0Count3;

    TL0 = 0x18; // 设置定时初始值
    TH0 = 0xFC; // 设置定时初始值
    T0Count1++;
    if (T0Count1 >= 10) {
        T0Count1 = 0;
        Key_Loop();
    }
    T0Count2++;
    if (T0Count2 >= 1) {
        T0Count2 = 0;
        Nixie_Loop();
    }
    T0Count3++;
    if (T0Count3 >= 10) {
        T0Count3 = 0;
        Sec_Loop();
    }
}