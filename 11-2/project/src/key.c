#include "REG52.H"
#include "key.h"

unsigned char ket_get;

unsigned char Key()
{
    unsigned char Temp = 0;

    Temp   = ket_get;
    ket_get = 0;
    return Temp;
}

/// @brief 获取独立按键键码
/// @return 按下的键码，范围0~4，无按键返回0
unsigned char Key_GetState()
{
    unsigned char KeyNumber = 0;

    if (P3_1 == 0) {
        KeyNumber = 1;
    }
    if (P3_0 == 0) {
        KeyNumber = 2;
    }
    if (P3_2 == 0) {
        KeyNumber = 3;
    }
    if (P3_3 == 0) {
        KeyNumber = 4;
    }

    return KeyNumber;
}

void Key_Loop()
{
    static unsigned char NowState, LastState;
    LastState = NowState;
    NowState  = Key_GetState();

    if (LastState == 1 && NowState == 0) {
        ket_get = 1;
    }
    if (LastState == 2 && NowState == 0) {
        ket_get = 2;
    }
    if (LastState == 3 && NowState == 0) {
        ket_get = 3;
    }
    if (LastState == 4 && NowState == 0) {
        ket_get = 4;
    }
}