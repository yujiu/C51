#include "REG52.H"
#include "I2C.h"

sbit I2C_SCL = P2 ^ 1;
sbit I2C_SDA = P2 ^ 0;

/// @brief 从I2C接收应答
/// @return 应答位，0为应答，1为非应答
bit I2C_ReceiveAck(void)
{
    bit AckBit = 0;
    I2C_SDA    = 1;
    I2C_SCL    = 1;
    AckBit     = I2C_SDA;
    I2C_SCL    = 0;

    return AckBit;
}

/// @brief 向I2C发送应答
/// @param Ackbit 应答位，0为应答，1为非应答
void I2C_SendAck(bit Ackbit)
{
    I2C_SDA = Ackbit;
    I2C_SCL = 1;
    I2C_SCL = 0;
}

/// @brief  从I2C接收一个字节
/// @return 接收的字节
unsigned char I2C_ReceiveByte()
{
    unsigned char Byte = 0x00;
    unsigned char i    = 0;

    I2C_SDA = 1;

    for (i = 0; i < 8; i++) {
        I2C_SCL = 1;
        if (I2C_SDA == 1) {
            Byte |= (0x80 >> i);
        }
        I2C_SCL = 0;
    }

    return Byte;
}

/// @brief 向I2C发送一个字节
/// @param Byte 发送的字节
void I2C_SendByte(unsigned char Byte)
{
    unsigned char i = 0;

    for (i = 0; i < 8; i++) {
        I2C_SDA = Byte & (0x80 >> i);
        I2C_SCL = 1;
        I2C_SCL = 0;
    }
}

/// @brief I2C结束
void I2C_Start()
{
    I2C_SDA = 1;
    I2C_SCL = 1;

    I2C_SDA = 0;
    I2C_SCL = 0;
}

/// @brief I2C开始
void I2C_Stop()
{
    I2C_SDA = 0;

    I2C_SCL = 1;
    I2C_SDA = 1;
}