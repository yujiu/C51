#include <REGX52.H>

void Delay(unsigned int xms)	//@12.000MHz
{
	unsigned char data i, j;

	while((xms--)!=0)
	{
		i = 2;
		j = 239;
		do
		{
			while (--j);
		} while (--i);
	}
}

void main()
{
	while(1)
	{
			P2=0xFE;
			Delay(200);
			P2=0xFD;
			Delay(200);
			P2=0xFB;
			Delay(200);
			P2=0xF7;
			Delay(200);
	}
}
