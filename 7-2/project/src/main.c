#include "REG52.H"
#include "Delay.h"
#include "LCD1602.h"
#include "MatrixKey.h"
#include "Timer0.h"
#include "key.h"
#include "INTRINS.H"

unsigned char KeyNumber = 0;
unsigned char LEDMode   = 0;

void main()
{
    P2 = 0xFE;
    Timer0_Init();
    while (1) {
        KeyNumber = Key();
        if (KeyNumber != 0) {
            if (KeyNumber == 1) {
                LEDMode++;
                if (LEDMode >= 2) {
                    LEDMode = 0;
                }
            }
            if (KeyNumber == 2) P2_2 = ~P2_2;
            if (KeyNumber == 3) P2_3 = ~P2_3;
            if (KeyNumber == 4) P2_4 = ~P2_4;
        }
    }
}

void Timer0_Routine() interrupt 1
{
    static unsigned int T0Count = 0;

    TL0 = 0x18; // 设置定时初始值
    TH0 = 0xFC; // 设置定时初始值
    T0Count++;
    if (T0Count >= 500) {
        T0Count = 0;
        if (LEDMode == 0) {
            P2 = _crol_(P2, 1);
        }
        if (LEDMode == 1) {
            P2 = _cror_(P2, 1);
        }
    }
}