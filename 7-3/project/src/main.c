#include "REG52.H"
#include "Delay.h"
#include "LCD1602.h"
#include "MatrixKey.h"
#include "Timer0.h"

unsigned char Sec  = 50;
unsigned char Min  = 59;
unsigned char Hour = 23;
void main()
{
    int Result = 0;
    Timer0_Init();
    LCD_Init();
    LCD_ShowString(1, 1, "Clock:");
    LCD_ShowChar(2, 3, ':');
    LCD_ShowChar(2, 6, ':');
    while (1) {
        LCD_ShowNum(2, 1, Hour, 2);
        LCD_ShowNum(2, 4, Min, 2);
        LCD_ShowNum(2, 7, Sec, 2);
    }
}

void Timer0_Routine() interrupt 1
{
    static unsigned int T0Count = 0;

    TL0 = 0x18; // 设置定时初始值
    TH0 = 0xFC; // 设置定时初始值
    T0Count++;
    if (T0Count >= 1000) {
        T0Count = 0;
        Sec++;
        if (Sec >= 60) {
            Sec = 0;
            Min++;
            if (Min >= 60) {
                Min = 0;
                Hour++;
                if (Hour >= 24) {
                    Hour = 0;
                }
            }
        }
    }
}