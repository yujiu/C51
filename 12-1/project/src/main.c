#include "REG52.H"
#include "key.h"
#include "Timer0.h"
#include "Delay.h"
#include "Nixie.h"

sbit MOTOR = P1 ^ 0;

unsigned char Counter, Compare; // 计数值和比较值，用于输出PWM
unsigned char KeyNum, Speed;

void main()
{
    Timer0_Init();
    while (1) {
        KeyNum = Key();
        if (KeyNum == 1) {
            Speed++;
            Speed %= 4;
            if (Speed == 0) { Compare = 0; } // 设置比较值，改变PWM占空�?
            if (Speed == 1) { Compare = 50; }
            if (Speed == 2) { Compare = 75; }
            if (Speed == 3) { Compare = 100; }
        }
        Nixie(1, Speed);
    }
}

void Timer0_Routine() interrupt 1
{
    TL0 = 0x9C; // 设置定时初�?
    TH0 = 0xFF; // 设置定时初�?
    Counter++;
    Counter %= 100;        // 计数值变化范围限制在0~99
    if (Counter < Compare) // 计数值小于比较�?
    {
        MOTOR = 1; // 输出1
    } else         // 计数值大于比较�?
    {
        MOTOR = 0; // 输出0
    }
}
