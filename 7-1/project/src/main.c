#include "REG52.H"
#include "Delay.h"
#include "LCD1602.h"
#include "MatrixKey.h"
#include "Timer0.h"

void main()
{
    int Result = 0;
    Timer0_Init();
    while (1) {
    }
}


void Timer0_Routine() interrupt 1
{
    static unsigned int T0Count = 0;

    TL0 = 0x18; // 设置定时初始值
    TH0 = 0xFC; // 设置定时初始值
    T0Count++;
    if (T0Count >= 1000) {
        T0Count = 0;
        P2_0    = ~P2_0;
    }
}