#include "REG52.H"
#include "Delay.h"
#include "LCD1602.h"
#include "UART.h"

unsigned char Sec = 0;

void main()
{
    int Result = 0;
    LCD_Init();
    LCD_ShowString(1, 1, "HelloWorld!");
    Uart_Init();
    while (1) {
        Uart_SendByte(Sec);
        Sec++;
        Delay(1000);
    }
}
