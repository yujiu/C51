#include "REG52.H"
#include "Delay.h"
#include "LCD1602.h"
#include "MatrixKey.h"

void main()
{
    int Result = 0;
    LCD_Init();
    LCD_ShowString(1, 1, "HelloWorld!");
    while (1) {
        Result = MatrixKey();
        if (Result != 0) {
            LCD_ShowNum(2, 1, Result, 3);
        }
    }
}
