#include "REG52.H"
#include "Delay.h"
#include "LCD1602.h"

sbit SER = P3 ^ 4; // SER
sbit RCK = P3 ^ 5; // RCLK
sbit SCK = P3 ^ 6; // SRCLK

void _74HC595_WriteByte(unsigned char Byte);
void MatrixLED_ShowColumn(unsigned char Column, Data);

void main()
{
    SCK = 0;
    RCK = 0;

    while (1) {
        MatrixLED_ShowColumn(0, 0x3C);
        MatrixLED_ShowColumn(1, 0x42);
        MatrixLED_ShowColumn(2, 0xA9);
        MatrixLED_ShowColumn(3, 0x85);
        MatrixLED_ShowColumn(4, 0x85);
        MatrixLED_ShowColumn(5, 0xA9);
        MatrixLED_ShowColumn(6, 0x42);
        MatrixLED_ShowColumn(7, 0x3C);
    }
}

/// @brief          LED点阵屏显示一列数据
/// @param Column   要选的列，范围0-7
/// @param Data     选择列显示的数据，高位在上 1亮
void MatrixLED_ShowColumn(unsigned char Column, unsigned char Data)
{
    _74HC595_WriteByte(Data);
    P0 = ~(0x80 >> Column);
    Delay(1);
    P0 = 0xFF;
}

/// @brief  74HC595写入一个字节
/// @param Byte 写入的字节
void _74HC595_WriteByte(unsigned char Byte)
{
    unsigned char i;
    for (i = 0; i < 8; i++) {
        SER = Byte & (0x80 >> i);
        SCK = 1;
        SCK = 0;
    }
    RCK = 1;
    RCK = 0;
}
