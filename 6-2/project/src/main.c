#include "REG52.H"
#include "Delay.h"
#include "LCD1602.h"
#include "MatrixKey.h"

void main()
{
    unsigned char Result  = 0;
    unsigned int Password = 0;
    unsigned int Count    = 0;
    LCD_Init();
    LCD_ShowString(1, 1, "HelloWorld!");
    while (1) {
        Result = MatrixKey();
        if (Result != 0) {
            if (Result <= 10) {
                if (Count < 4) {
                    Password *= 10;
                    Password += Result % 10;
                    LCD_ShowNum(2, 1, Password, 4);
                    Count++;
                }
            }
            if (Result == 11) {
                if (Password == 1234) {
                    LCD_ShowString(1, 14, "OK!");
                } else {
                    LCD_ShowString(1, 14, "err");
                    Password = 0;
                    Count    = 0;
                    LCD_ShowNum(2, 1, Password, 4);
                }
            }
            if (Result == 12) {
                Password = 0;
                Count    = 0;
                LCD_ShowString(1, 14, "   ");
                LCD_ShowNum(2, 1, Password, 4);
            }
        }
    }
}
