#include "REG52.H"
#include "INTRINS.H"

sbit Buzzer = P1 ^ 5;

void Delay500us(void) //@12.000MHz
{
    unsigned char data i;

    _nop_();
    i = 247;
    while (--i);
}

void Buzzer_Time(unsigned int ms)
{
    ms *= 2;
    for (; ms != 0; ms--) {
        Buzzer = !Buzzer;
        Delay500us();
    }
}