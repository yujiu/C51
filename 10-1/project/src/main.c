#include "REG52.H"
#include "Delay.h"
#include "key.h"
#include "Nixie.h"
#include "Buzzer.h"

unsigned char KeyNum = 0;
unsigned int i       = 0;

void main()
{
    Nixie(1, 0);
    while (1) {
        KeyNum = Key();
        if (KeyNum) {
            Buzzer_Time(100);
            Nixie(1, KeyNum);
        }
    }
}
